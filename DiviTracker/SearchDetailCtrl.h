//
//  SearchDetailCtrl.h
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-07.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDetailCtrl : UITableViewController

@property (strong, nonatomic) NSDictionary *dictJson2;
@property (strong, nonatomic) NSDictionary *dictJson3;

@end
