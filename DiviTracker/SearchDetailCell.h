//
//  SearchDetailCell.h
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-10.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblExch;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTick;
@property (weak, nonatomic) IBOutlet UILabel *lblType;

@end
