//
//  SearchCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-07.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "SearchCtrl.h"
#import "SearchDetailCtrl.h"
#import "PortfolioCtrl.h"

@interface SearchCtrl ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (strong, nonatomic) NSDictionary *dictJson;
@property (strong, nonatomic) IBOutlet UILabel *lblError;

@end

@implementation SearchCtrl

- (IBAction)btnSearchPressed:(id)sender {
    
    self.lblError.hidden = TRUE;
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = FALSE;
    
    [self createSearchObj: self.searchField.text];
    
    NSLog(@"SearchCtrl btnSearchPressed searchField: %@", self.searchField.text);
    
}


- (NSString *) jsonpToString: (NSData*)data {
    
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"SearchCtrl jsonString: %@", jsonString);
    
    NSRange range   = [jsonString rangeOfString:@"("];
    range.location++;
    range.length    = [jsonString length] - range.location - 1; // removes parens and trailing semicolon
    jsonString      = [jsonString substringWithRange:range];
    
    return jsonString;
    
}
- (void) errorMSG {
    
    self.lblError.text = [NSString stringWithFormat:@"%@ is not a usable character", self.searchField.text];
    
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Wrong" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
  //  [messageAlert show];
    [self.activityIndicator stopAnimating];
    self.lblError.hidden = FALSE;
    
}
- (void)createSearchObj: (NSString*)searchStr {
    

    NSLog(@"SearchCtrl searchStr: %@", searchStr);
    
    NSString *searchString = [NSString stringWithFormat: @"http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=%@&callback=YAHOO.Finance.SymbolSuggest.ssCallback", searchStr];
    
    
    
    NSURL                *url     = [NSURL URLWithString:searchString];
    NSURLRequest         *request = [NSURLRequest requestWithURL:url];
    NSURLSession         *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task    = [session dataTaskWithRequest:request completionHandler:^
                                     (NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         
                                         //NSLog(@"DATA: %@", data);
                                         if(error) {
                                             NSLog(@"SearchCtrl ERROR: %@", error);
                                             //[self errorMSG];
                                             
                                         }
                                        /*
                                         NSDictionary *root =
                                         [NSJSONSerialization JSONObjectWithData:
                                         data options:kNilOptions error: &parsingError];
                                        */
                                         
                                         if (data && !error) {
                                             //NSLog(@"DATA: %@", data);
                                             
                                             //parse jsonp data to a string
                                             NSString   *jsonString = [self jsonpToString: data];
                                             NSData     *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                                             NSError    *jsonError = nil;
                                             //This is the json object
                                             NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                        options:NSJSONReadingMutableContainers error:&jsonError];
                                            // NSLog(@"jsonResponse: %@", jsonResponse);
                                             
                                             if (jsonResponse) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     
                                                    self.dictJson = [jsonResponse objectForKey:@"ResultSet"];
                                                   
                                                     NSLog(@"SearchCtrl dictJson (A dictionary containing an array which contains a dictionary): %@",self.dictJson);/*
                                                     NSLog(@"objforkey: %@", [self.dictJson objectForKey:@"Result"]);
                                                     NSLog(@"ARRAY objforkey: %@", [self.dictJson objectForKey:@"Result"][0]);
                                                     NSLog(@"valueForKeyPath: %@", [self.dictJson valueForKeyPath:@"Result.name"]);
                                                     NSLog(@"valueForKeyPath: %@", [self.dictJson valueForKeyPath:@"Result.symbol"]);
                                                    */
                                                     [self performSegueWithIdentifier:@"searchToDetail" sender:self];
                                                     
                                                 });
                                                 
                                             } else {
                                                 NSLog(@"Unable to parse JSON data: %@", jsonError);
                                             }
                                         } else {
                                             NSLog(@"Error loading data: %@", error);
                                         }


                                     }];

    [task resume];
    [self.view endEditing:YES];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [self.activityIndicator stopAnimating];
    self.lblError.hidden = TRUE;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"searchToDetail"]) {

        SearchDetailCtrl    *nextCtrl = [segue destinationViewController];
        SearchCtrl          *thisCtrl = sender; // whats this good for? self?
        
        nextCtrl.dictJson2 =  thisCtrl.dictJson;        
        
 //       nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"searchToPortfolio"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
    }
    
    
}


@end
