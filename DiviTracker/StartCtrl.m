//
//  StartCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-05-29.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "StartCtrl.h"
#import "StockCtrl.h"

@interface StartCtrl ()

@property (weak, nonatomic) IBOutlet UITextField *searchField;

@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (weak, nonatomic) IBOutlet UIButton *btnPortfolio;

@property (strong, nonatomic) NSDictionary *dict3; // stock object to send


@end




@implementation StartCtrl


- (IBAction)btnSearchPressed:(id)sender {
    
    NSLog(@"btnSearchPressed Called");
    
    [self createStockObj: self.searchField.text];
    
}

- (IBAction)btnPortfolioPressed:(id)sender {
    NSLog(@"btnPortfolioPressed Called");
}


- (void)createStockObj: (NSString*)stockName {
    
    NSLog(@"createStockObj called");
    NSLog(@"stockName: %@", stockName);
    /*
     NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22STAR.ST%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
     */
//    NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22XOM%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    
    // % in url interferes with stringWithFormat. Easiest way is to break up into 3 strings
    NSString *searchStr1 = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";
    NSString *searchStr2 = @"%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    NSString *searchString = [NSString stringWithFormat: @"%@%@%@", searchStr1,stockName,searchStr2];
    
    
    NSURL                *url     = [NSURL URLWithString:searchString];
    NSURLRequest         *request = [NSURLRequest requestWithURL:url];
    NSURLSession         *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task    = [session dataTaskWithRequest:request completionHandler:^
                                    (NSData *data, NSURLResponse *response, NSError *error)
                                    {
                                        
                                        NSLog(@"ERROR: %@", error);
                                                   
                                        NSError *parsingError = error;
                                                   
                                        NSDictionary *root =
                                        [NSJSONSerialization JSONObjectWithData:
                                        data options:kNilOptions error: &parsingError];
                                                   
                                        if (!parsingError) {
                                            NSLog(@"Start Parsing Json");
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                
                                            //Put the query json obj in a dict.
                                            NSDictionary *dict = [root objectForKey:@"query"];
                                            // "created" is in the first dict
                                            NSLog(@"dict: created: %@", [dict objectForKey:@"created"]);
                                            // put the results of the query in dict2
                                            NSDictionary *dict2 = [dict objectForKey:@"results"];
                                            // Now we got the stock info, put it in dict3
                                            self.dict3 = [dict2 objectForKey:@"quote"];
                                                
                                            
                                                           NSLog(@"Dict3: %@", self.dict3);
                                                           /*
                                                            NSMutableDictionary *mDict = [dict2 objectForKey:@"quote"];
                                                            NSLog(@"MUTABLE DICTIONARY!");
                                                            NSLog(@"mDict Ticker: %@", [mDict objectForKey:@"Symbol"]);
                                                            NSLog(@"mDict Name: %@", [mDict objectForKey:@"Name"]);
                                                            */
                                                           
                                            NSLog(@"StartCtrl Ticker: %@", [self.dict3 objectForKey:@"Symbol"]);
                                            NSLog(@"StartCtrl Name: %@",   [self.dict3 objectForKey:@"Name"]);
                                                
                                                           /*
                                                           NSLog(@"Ask: %@", [dict3 objectForKey:@"Ask"]);
                                                           NSLog(@"Change: %@", [dict3 objectForKey:@"PercentChange"]);
                                                           NSLog(@"YearRange: %@", [dict3 objectForKey:@"YearRange"]);
                                                           NSLog(@"PERatio: %@", [dict3 objectForKey:@"PERatio"]);
                                                           
                                                           NSLog(@"Yield: %@", [dict3 objectForKey:@"DividendYield"]);
                                                           NSLog(@"Dividend: %@", [dict3 objectForKey:@"DividendShare"]);
                                                           NSLog(@"Currency: %@", [dict3 objectForKey:@"Currency"]);
                                                           
                                                           NSLog(@"Payout Date: %@", [dict3 objectForKey:@"DividendPayDate"]);
                                                           NSLog(@"ExDividendDate: %@", [dict3 objectForKey:@"ExDividendDate"]);
                                                            */
                                            // Asynchronous, call new ctrl only after data collected
                                            [self performSegueWithIdentifier:@"startToStock" sender:self];
                                                
                                                       });
                                                       
                                                   } else {
                                                       
                                                       NSLog(@"Couldnot parse json: %@", parsingError);
                                                       
                                                   }
                                                   
                                               }];
    
    [task resume];
    [self.view endEditing:YES];

}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"prepareForSegue Called");
  
    if([segue.identifier isEqualToString:@"startToStock"]) {
        
        StockCtrl *nextCtrl = [segue destinationViewController];
        StartCtrl *thisCtrl = sender; // whats this good for? self?
        
        
        nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"favorites"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
    }
    
}


#pragma mark - iOS Stuff

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"StartCtrl viewDidLoad called");
    

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
