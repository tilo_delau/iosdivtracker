//
//  main.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-05-29.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
