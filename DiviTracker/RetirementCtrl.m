//
//  RetirementCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-28.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "RetirementCtrl.h"

@interface RetirementCtrl ()


@property (strong, nonatomic) IBOutlet UILabel *lblMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalStocks;
@property (strong, nonatomic) IBOutlet UILabel *lblDivValue;

@property (strong, nonatomic) IBOutlet UILabel *lblPortYield;
@property (strong, nonatomic) IBOutlet UITextField *inputGoal;

@property (strong, nonatomic) IBOutlet UILabel *lblGoal;

@property (strong, nonatomic) NSMutableArray    *arrPortfolio;
@property (strong, nonatomic) NSString          *searchString;
@property (strong, nonatomic) NSString          *stockName;
@property (strong, nonatomic) NSDictionary      *dictStockObj;
@property (strong, nonatomic) NSArray           *arrStockObj;


@end

@implementation RetirementCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    self.inputGoal.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"Really? Memory Warning?");
}

- (void) viewWillAppear:(BOOL)animated {
    NSLog(@"RetirementCtrl viewWillAppear called");
    [self loadUserDefaults];
    NSLog(@"PortfolioCtrl arrPortfolio: %@", self.arrPortfolio);
    
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    
    if(self.arrPortfolio.count) {
        NSLog(@"portfolio is not empty %lu, %@", (unsigned long)self.arrPortfolio.count, self.arrPortfolio);
        self.lblTotalStocks.text =[NSString stringWithFormat:@"%lu", (unsigned long)self.arrPortfolio.count];
        [self createStockName];
        [self createStockObj];
    }
    else { NSLog(@"Retirement Portfolio is empty"); self.lblMsg.text = @"Your Portfolio is empty"; };
    
}
// Dont need to save anything in this controller
- (void) saveUserDefaults {
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject: self.arrPortfolio forKey:@"portfolio"];
    // Save to file
    [userDefaults synchronize];
    NSLog(@"PortfolioCtrl: Saving Portfolio: %@", self.arrPortfolio);
    
}
- (void) loadUserDefaults {
    
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    NSLog(@"PortfolioCtrl: userDefaults: %@", [userDefaults objectForKey:@"portfolio"]);
   
    self.arrPortfolio = [[userDefaults arrayForKey:@"portfolio"] mutableCopy];
    
}
- (IBAction)inputGoal:(id)sender {
    
    NSLog(@"inputGoal called");
    
    [self.inputGoal resignFirstResponder];
    [self viewWillAppear:YES];
}
- (IBAction)btnGo:(id)sender {
    
    [self.inputGoal resignFirstResponder];
    [self viewWillAppear:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.inputGoal) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)createStockName {
    
    self.stockName = @"";
    
    for(int i=0; i < self.arrPortfolio.count; i++) {
        
        if(i > 0) self.stockName =  [self.stockName stringByAppendingString:@"+"];
        
        self.stockName = [self.stockName stringByAppendingString:[self.arrPortfolio objectAtIndex:i]];
    }
}

- (void)createStockObj {
    
    NSLog(@"createStockObj called");
    NSLog(@"stockName: %@", self.stockName);
    
    // % in url interferes with stringWithFormat. Easiest way is to break up into 3 strings
    NSString *searchStr1 = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";
    NSString *searchStr2 = @"%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    NSString *searchString = [NSString stringWithFormat: @"%@%@%@", searchStr1,self.stockName,searchStr2];
    
    
    NSURL                *url     = [NSURL URLWithString:searchString];
    NSURLRequest         *request = [NSURLRequest requestWithURL:url];
    NSURLSession         *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task    = [session dataTaskWithRequest:request completionHandler:^
                                     (NSData *data, NSURLResponse *response, NSError *parsingError)
                                     {
                                         
                                         NSDictionary *root =
                                         [NSJSONSerialization JSONObjectWithData:
                                          data options:kNilOptions error: &parsingError];
                                         
                                         if (!parsingError) {
                                             NSLog(@"Start Parsing Json");
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 //Put the query json obj in a dict.
                                                 NSDictionary *dict = [root objectForKey:@"query"];
                                                 
                                                 // "created" is in the first dict
                                                 NSLog(@"dict: created: %@", [dict objectForKey:@"created"]);
                                                 
                                                 // put the results of the query in dict2
                                                 NSDictionary *dict2 = [dict objectForKey:@"results"];
                                                 
                                                 // If there is just 1 stock, json is a dict
                                                 if(self.arrPortfolio.count < 2) {
                                                     self.dictStockObj = [dict2 objectForKey:@"quote"];
                                                 } else {
                                                     // multiple stocks, json is an array (with dicts)
                                                     self.arrStockObj   = [dict2 objectForKey:@"quote"];
                                                    
                                                 }
                                                 
                                                 NSLog(@"RetirementCtrl: total stocks: %lu", (unsigned long)self.arrPortfolio.count);
                                                 
                                                 [self calculateDividends];
                                                 [self.activityIndicator stopAnimating];
                                                 
                                             });
                                             
                                             
                                         } else {
                                             
                                             NSLog(@"Couldnot parse json: %@", parsingError);
                                             
                                         }
                                         
                                     }];
    
    [task resume];
 //   [self.view endEditing:YES];
    
}

- (void) calculateGoal {
    
    float inputG;
    float yield;
    float goal;
    
    yield   = [self.lblPortYield.text floatValue];
    inputG  = [self.inputGoal.text floatValue];
    
    yield   = yield / 100;
    
    NSLog(@"yield: %f", yield);
    NSLog(@"inputG: %f", inputG );
    
    goal = inputG / yield;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:goal]];
    self.lblGoal.text = [NSString stringWithFormat:@"%@", numberAsString];
    
    numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:inputG]];
    self.inputGoal.text = @"0";
    self.inputGoal.text = [NSString stringWithFormat:@"%@", numberAsString];
}

- (void) calculateDividends {
    NSLog(@"calculateDividends called");
    NSDictionary *dictOneStock;
    
    self.lblMsg.text = @"";
    float totDividends = 0.0;
    float portValue    = 0.0;
    
    
    for(int i=0; i < self.arrPortfolio.count; i++) {
        NSLog(@"calc count %d", i);
        
        if(self.arrPortfolio.count > 1) dictOneStock = self.arrStockObj[i];
        else dictOneStock  = self.dictStockObj;
//        NSLog(@"dictOneStock %d: %@", i, dictOneStock);
        
        if(![[dictOneStock objectForKey:@"DividendShare"] isEqual:[NSNull null]]) {
            //NSLog(@"writelabel %@ is not empty: %@", stockQuery, [self.dictJson4 objectForKey: stockQuery]);
            self.lblDivValue.text = [dictOneStock objectForKey:@"DividendShare"];
        } else self.lblDivValue.text = @"0";
        
        
        NSLog(@"Dividend Value %@: %@", [dictOneStock objectForKey:@"Symbol"], self.lblDivValue.text);
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *myNumber = [f numberFromString:self.lblDivValue.text];

//        NSNumber *myNumber = [f numberFromString:[dictOneStock objectForKey:@"DividendShare"]];

        
        float stockDiv = ([myNumber floatValue]);
        totDividends = totDividends + stockDiv;
        
        myNumber = [f numberFromString:[dictOneStock objectForKey:@"PreviousClose"]];
        float stockValue = ([myNumber floatValue]);
        portValue = portValue + stockValue;
        
        
    }
    self.lblDivValue.text  = [NSString stringWithFormat:@"$%.2f", totDividends];
    self.lblPortYield.text = [NSString stringWithFormat:@"%.2f%@", totDividends / portValue * 100, @"%"];

    [self calculateGoal];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
