//
//  AppDelegate.h
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-05-29.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

