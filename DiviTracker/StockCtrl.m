//
//  StockCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-06-14.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "StockCtrl.h"
#import "SearchDetailCtrl.h"
#import "PortfolioCtrl.h"

@interface StockCtrl ()
@property (weak, nonatomic)     IBOutlet UILabel *lblTicker;

@property (weak, nonatomic)     IBOutlet UILabel *lblStockName;
@property (strong, nonatomic)   IBOutlet UILabel *lblAsk;
@property (strong, nonatomic)   IBOutlet UILabel *lblAskPrice;
@property (weak, nonatomic)     IBOutlet UILabel *lblYieldValue;
@property (weak, nonatomic)     IBOutlet UILabel *lblDividendValue;
@property (weak, nonatomic)     IBOutlet UILabel *lblPayoutDate;

@property (weak, nonatomic)     IBOutlet UILabel *lblMsg;

@property (weak, nonatomic)     IBOutlet UIButton *btnAdd;

@property (strong, nonatomic)   NSMutableArray *portfolio;
@property (strong, nonatomic)   NSMutableArray *portData;

@end


@implementation StockCtrl


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"StockCtrl viewDidLoad called");
    
    self.lblAskPrice.text       =   @"";
    self.lblYieldValue.text     =   @"";
    self.lblDividendValue.text  =   @"";
    self.lblPayoutDate.text     =   @"";
    
    if([[self.dictJson4 objectForKey:@"DividendShare"]  isEqual:[NSNull null]]) {
    
        self.lblMsg.text        =   @"Stock does not pay a dividend";
        
    }
    self.lblTicker.text         = [self checkData: @"Symbol"];
    self.lblStockName.text      = [self checkData: @"Name"];
    self.lblAskPrice.text       = [NSString stringWithFormat: @"%@ %@",
                                  [self checkData: @"PreviousClose"],
                                  [self checkData: @"Currency"]];
    self.lblYieldValue.text     = [NSString stringWithFormat: @"%@ %@",
                                  [self checkData: @"DividendYield"], @"%"];
    self.lblDividendValue.text  = [NSString stringWithFormat: @"%@ %@",
                                  [self checkData: @"DividendShare"],
                                  [self checkData: @"Currency"]];
    self.lblPayoutDate.text     = [self checkData: @"DividendPayDate"];
}

- (NSString*) checkData: (NSString *) stockQuery {
    
    if(![[self.dictJson4 objectForKey: stockQuery] isEqual:[NSNull null]]) {
        //NSLog(@"writelabel %@ is not empty: %@", stockQuery, [self.dictJson4 objectForKey: stockQuery]);
        return [self.dictJson4 objectForKey: stockQuery];
    } else return @"No Data";
           
}

- (void) addStock {
    NSUserDefaults      *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictPort1    = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictPort2    = [[NSMutableDictionary alloc]init];
    
    self.portData = [[userDefaults arrayForKey:@"portfolio"] mutableCopy];
    
    if(self.portData == nil || [self.portData count] == 0) {
        NSLog(@"portData is empty");
        
        self.portData = [[NSMutableArray alloc] init];
    }
    
    
}

- (IBAction)btnAddPressed:(id)sender {
    
    [self addStock];
 /*
    NSMutableDictionary *dictPort1    = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictPort2    = [[NSMutableDictionary alloc]init];
    NSMutableArray      *arrPort      = [[NSMutableArray alloc]init];
  */
    NSUserDefaults      *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.portfolio = [[userDefaults arrayForKey:@"portfolio"] mutableCopy];
    
    if(self.portfolio == nil || [self.portfolio count] == 0) {
        NSLog(@"portfolio is empty");
        
        self.portfolio = [[NSMutableArray alloc] init];
    }
  
   // if([self.portfolio valueForKey: self.lblTicker.text]) {
    NSLog(@"self.portfolio: %@", self.portfolio);
    if ([self.portfolio containsObject:self.lblTicker.text]) {
        self.lblMsg.text = [NSString stringWithFormat: @"%@ is already in your portfolio", self.lblTicker.text];
    } else {
        
  //  self.dictJson = [jsonResponse objectForKey:@"ResultSet"];
    
  //      dictPort1 setObject: [self.portfolio containsObject:self.lblTicker.text];
/*
    dictPort1 setObject: [self.portfolio containsObject:self.lblTicker.text] forKey:self.lblTicker.text;
    
        //
        
    [self.portfolio addObject:<#(id)#>]
    [dictPort2 setObject:self.lblTicker.text forKey:@"ticker"];
    [dictPort2 setValue: @"100"              forKey:@"shares"];
    [dictPort2 setValue: [NSDate date]       forKey:@"buyDate"];
        
 //   if ([self.portfolio containsObject:self.lblTicker.text]) {
        
    [arrPort addObject: dictPort2];
    [dictPort1 setObject:arrPort forKey:self.lblTicker.text];
    [self.portfolio addObject: dictPort1];
        
    NSLog(@"arrPort: %@", arrPort);
*/
  //  NSLog(@"dictPort: %@", dictPort1);
/*
        NSLog(@"dictPort ticker: %@", self.dictPort1[self.lblTicker.text]);
    NSLog(@"dict valueForKey: %@", [self.dictPort valueForKey:@"T"]);
    NSLog(@"dict objectForKey: %@", [self.dictPort objectForKey:@"T"]);
*/
    
    [self.portfolio addObject: self.lblTicker.text];
//    [self.portfolio addObject: dictPort1];

  //  NSLog(@"dictport2: %@", [dictPort2 objectForKey:self.lblTicker.text]);
 //   NSLog(@"shares: %@", [[self.portfolio objectAtIndex:self.portfolio.count-1] objectForKey:@"shares"]);
//    NSLog(@"shares: %@", [self.portfolio[self.portfolio.count-1 [stockData objectForKey:@"Category"] objectForKey:@"shares"]);
        

        
    // Write over the old portfolio
    [userDefaults setObject: self.portfolio forKey:@"portfolio"];
    // Save to file
    const BOOL didSave = [userDefaults synchronize];
    
    self.lblMsg.text = [NSString stringWithFormat: @"%@ added to Portfolio", self.lblTicker.text];
        
        NSLog(@"StockCtrl Check Portfolio userDefaults: %@", [userDefaults objectForKey:@"portfolio"]);
    
        if (!didSave) {
            self.lblMsg.text = @"Portfolio could not be saved.";
            NSLog(@"StockCtrl Portfolio could not be saved");
        }
    }
    
    
    
}


- (IBAction)btnCheckPortfolio:(id)sender {
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    NSLog(@"StockCtrl Check Portfolio userDefaults: %@", [userDefaults objectForKey:@"portfolio"]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnViewPortfolioPressed:(id)sender {
    [self performSegueWithIdentifier:@"stockToPortfolio" sender:self];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSLog(@"prepareForSegue Called");
    
    if([segue.identifier isEqualToString:@"stockToPortfolio"]) {
        
        PortfolioCtrl       *nextCtrl = [segue destinationViewController];
        SearchDetailCtrl    *thisCtrl = sender; // whats this good for? self?
        
      //  nextCtrl.dictJson4 =  thisCtrl.dictJson3;
        
        NSLog(@"Prepared");
        
        
        //       nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"favorites"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
    }
    
    
}
@end
