//
//  SearchDetailCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-07.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "SearchDetailCtrl.h"
#import "SearchDetailCell.h"
#import "StockCtrl.h"



@interface SearchDetailCtrl ()

@property (strong, nonatomic) NSArray *arrJsonResult;


@end

@implementation SearchDetailCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"SearchDetailCtrl viewDidLoad");
    
    //Get the array from the dictionary
    self.arrJsonResult= [self.dictJson2 objectForKey:@"Result"];
    
    //Get the dictionary from the array
 //   NSDictionary *dictJsonResult = self.arrJsonResult[2];
    
 //   NSLog(@"arrJsonResult exch: %@", [dictJsonResult objectForKey:@"name"]);
    
 //   NSLog(@"SearchDetailCtrl dictJson2: %@", [self.dictJson2 valueForKeyPath:@"Result"]);
 //   NSLog(@"SearchDetailCtrl dictJson2OBJ: %@", [self.dictJson2 objectForKey:@"Result[0]"]);
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    return self.arrJsonResult.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];

    // pointer to each row, dont care about implicit conversion, xcode is dumb
    int index = indexPath.row;
    // pointer to each stock
    NSDictionary *dictJsonResult = self.arrJsonResult[index];
    //fill each cell with one stock data
    cell.lblExch.text = [dictJsonResult objectForKey:@"exchDisp"];
    cell.lblName.text = [dictJsonResult objectForKey:@"name"];
    cell.lblTick.text = [dictJsonResult objectForKey:@"symbol"];
    
 //   cell.textLabel.text = [dictJsonResult objectForKey:@"symbol"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
 //   cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;

    if (indexPath.row%2 == 0) {
        UIColor *altCellColor = [UIColor colorWithWhite:0.2 alpha:0.1];
        cell.backgroundColor = altCellColor;
        
       // cell.detailTextLabel.backgroundColor = altCellColor;
     //   cell.imageView.backgroundColor = altCellColor;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
  //  [messageAlert show];
    
    SearchDetailCell *cell = (SearchDetailCell *)[tableView cellForRowAtIndexPath:indexPath];

    NSLog(@"SearchDetailCtrl Cell Pressed: %@", cell.lblTick.text);
    
    //Lets get the stock info and then prepare for seque
    [self createStockObj:cell.lblTick.text];
    
}
/* Header method, it sucks dont use
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"myCell";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    
    // Yes color matching is awful
    self.navigationController.navigationBar.titleTextAttributes =
    [NSDictionary dictionaryWithObject:[UIColor blueColor] forKey:NSForegroundColorAttributeName];

    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
*/
/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}
 */

- (void)createStockObj: (NSString*)stockName {
    
    NSLog(@"SearchDetailCtrl createStockObj called");
    NSLog(@"SearchDetailCtrl stockName: %@", stockName);
    /*
     NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22STAR.ST%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
     */
    //    NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22XOM%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    
    // % in url interferes with stringWithFormat. Easiest way is to break up into 3 strings
    NSString *searchStr1 = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";
    NSString *searchStr2 = @"%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    NSString *searchString = [NSString stringWithFormat: @"%@%@%@", searchStr1,stockName,searchStr2];
    
    
    NSURL                *url     = [NSURL URLWithString:searchString];
    NSURLRequest         *request = [NSURLRequest requestWithURL:url];
    NSURLSession         *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task    = [session dataTaskWithRequest:request completionHandler:^
                                     (NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         
                                         NSLog(@"ERROR: %@", error);
                                         
                                         NSError *parsingError = error;
                                         
                                         NSDictionary *root =
                                         [NSJSONSerialization JSONObjectWithData:
                                          data options:kNilOptions error: &parsingError];
                                         
                                         if (!parsingError) {
                                             NSLog(@"SearchDetailCtrl Start Parsing Json");
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 //Put the query json obj in a dict.
                                                 NSDictionary *dict = [root objectForKey:@"query"];
                                                 NSLog(@"dict: %@", dict);
                                                 // "created" is in the first dict
                                                 //NSLog(@"SearchDetailCtrl dict: created: %@", [dict objectForKey:@"created"]);
                                                 // put the results of the query in dict2
                                                 NSDictionary *dict2 = [dict objectForKey:@"results"];
                                                 // Now we got the stock info, put it in dict3
                                                 self.dictJson3 = [dict2 objectForKey:@"quote"];
                                                 
                                                 NSLog(@"SearchDetailCtrl Dict2: %@", dict);
                                                 //NSLog(@"SearchDetailCtrl Dict3: %@", self.dictJson3);
                                                 /*
                                                  NSMutableDictionary *mDict = [dict2 objectForKey:@"quote"];
                                                  NSLog(@"MUTABLE DICTIONARY!");
                                                  NSLog(@"mDict Ticker: %@", [mDict objectForKey:@"Symbol"]);
                                                  NSLog(@"mDict Name: %@", [mDict objectForKey:@"Name"]);
                                                  */
                                                 
                                                 NSLog(@"SearchDetailCtrl Ticker: %@", [self.dictJson3 objectForKey:@"Symbol"]);
                                                 NSLog(@"SearchDetailCtrl Name: %@",   [self.dictJson3 objectForKey:@"Name"]);
                                                 
                                                 /*
                                                  NSLog(@"Ask: %@", [dict3 objectForKey:@"Ask"]);
                                                  NSLog(@"Change: %@", [dict3 objectForKey:@"PercentChange"]);
                                                  NSLog(@"YearRange: %@", [dict3 objectForKey:@"YearRange"]);
                                                  NSLog(@"PERatio: %@", [dict3 objectForKey:@"PERatio"]);
                                                  
                                                  NSLog(@"Yield: %@", [dict3 objectForKey:@"DividendYield"]);
                                                  NSLog(@"Dividend: %@", [dict3 objectForKey:@"DividendShare"]);
                                                  NSLog(@"Currency: %@", [dict3 objectForKey:@"Currency"]);
                                                  
                                                  NSLog(@"Payout Date: %@", [dict3 objectForKey:@"DividendPayDate"]);
                                                  NSLog(@"ExDividendDate: %@", [dict3 objectForKey:@"ExDividendDate"]);
                                                  */
                                                 // Asynchronous, call new ctrl only after data collected
                                                 [self performSegueWithIdentifier:@"searchDetailtoStock" sender:self];
                                                 
                                             });
                                             
                                         } else {
                                             
                                             NSLog(@"Couldnot parse json: %@", parsingError);
                                             
                                         }
                                         
                                     }];
    
    [task resume];
  //  [self.view endEditing:YES];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    if([segue.identifier isEqualToString:@"searchDetailtoStock"]) {
        
        StockCtrl           *nextCtrl = [segue destinationViewController];
        SearchDetailCtrl    *thisCtrl = sender; // whats this good for? self?
        
        nextCtrl.dictJson4 =  thisCtrl.dictJson3;
        
        //       nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"favorites"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
    }
    
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
