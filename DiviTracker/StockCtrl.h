//
//  StockCtrl.h
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-06-14.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface StockCtrl : UIViewController
@property (nonatomic) NSString *ticker;
@property (nonatomic) NSString *passStr;

@property (weak, nonatomic) IBOutlet UILabel *lblYield;
//@property (weak, nonatomic) IBOutlet UILabel *lblStockNameH;
//No longer using dict4
@property (strong, nonatomic) NSDictionary *dict4;
@property (strong, nonatomic) NSDictionary *dictJson4;

@end
